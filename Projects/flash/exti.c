// EXTI init functions
#include "stm32f429xx.h"

// Definitions
void InitEXTI(){
  // Remove interrupt mask from EXTI line 0
  EXTI->IMR |= EXTI_IMR_MR0;
  // Disable falling edge trigger
  EXTI->FTSR &= ~EXTI_IMR_MR0;
  // Enable rising edge trigger
  EXTI->RTSR |= EXTI_IMR_MR0;
}