///////// This code was written for the STM32F429-Discovery board
#include "stm32f429xx.h"
#include "init.h"

#define FLASH_KEY1 ((uint32_t)0x45670123)
#define FLASH_KEY2 ((uint32_t)0xCDEF89AB)
#define ADDR_FLASH_SECTOR_2 ((uint32_t)0x08008000)
#define MAXINT  4294967295   //2^32 - 1
#define MEM_SIZE 2097152     // 2MB

#define HWREG(x) (*(volatile uint32_t *)(x))

long offset = 0;
int power = 0;

// Function prototypes
void myFlashEraseSector(unsigned long sector);
void myFlashProgram(unsigned long address, unsigned long data);

void wait(int);
void blink();

// Entry point
int main(){
  int data;
  ////// Clocking
  // Enabling GPIOA and GPIOG
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOGEN;
  
  InitGPIO();
  InitEXTI();
  
  ///// FLASH
  // Unlocking Flash
  FLASH->KEYR = FLASH_KEY1;
  FLASH->KEYR = FLASH_KEY2;
  
  // If button is bushed, reset counter
  // WARNING! After hard reset (power on/off) there is spurious charge
  // and it seems that the button is pushed
  
  //if(GPIOA->IDR&GPIO_IDR_IDR_0){
  //  myFlashEraseSector(2);
  //  myFlashProgram(ADDR_FLASH_SECTOR_2, 0);
  //}
  
  // Enable interrupts from EXTI0 on NVIC
  NVIC_EnableIRQ(EXTI0_IRQn);
  
  // Searching for current offset
  while(HWREG(ADDR_FLASH_SECTOR_2 + offset) == 0)
    offset += 4;
    
  while(1) {
    wait(2500000);
    
    // The following code blinks as many times, as there are zeros in the flash
    data = HWREG(ADDR_FLASH_SECTOR_2 + offset);
    for(int i = 0; i < 8*offset; i++)
      blink();
    for(int i = 0; ((1 << i) & data) == 0; i++)
      blink();
  }
  
  return 0;
}


void wait(int iter_num){
  for (int i = 0; i < iter_num; i++);
}


void blink(){
  GPIOG->ODR ^= GPIO_ODR_ODR_14;
  wait(1000000);
  GPIOG->ODR ^=  GPIO_ODR_ODR_14;
  wait(1000000);
}


// Handle button click event
void EXTI0_IRQHandler(void)
{
  EXTI -> PR |= EXTI_IMR_MR0;     // Clear interrupt flag
  for(int i = 0; i < 1000000; i++);
  
  power += 1;
  if(power == 32){
    myFlashProgram(ADDR_FLASH_SECTOR_2 + offset, 0);
    offset += 4;
    if (offset  >= MEM_SIZE){
      myFlashEraseSector(2);
      offset = 0;
    }
    power = 0;
    return;
  }
  
  // write MAXINT - (2^power - 1)
  myFlashProgram(ADDR_FLASH_SECTOR_2 + offset, MAXINT - (1 << power) + 1);  
  return;
}


// Erase whole specified sector
void myFlashEraseSector(unsigned long sectorNumber)
{
  if(sectorNumber < 24)
  {
    while((FLASH->SR)&FLASH_SR_BSY);
    FLASH->CR |= FLASH_CR_SER; // Sector erase enable
    FLASH->CR &= ~FLASH_CR_SNB; // Set sector for erasing
    FLASH->CR |= (FLASH_CR_SNB&(sectorNumber<<3));
    FLASH->CR |= FLASH_CR_STRT; // Start erase operation
    while((FLASH->SR)&FLASH_SR_BSY);
    FLASH->CR &= ~FLASH_CR_SER; // Sector erase disable
  }
  else
  {
    while(1); // There are only 24 sectors
  }
}


// Program data value at specified address
void myFlashProgram(unsigned long address, unsigned long data)
{
  while((FLASH->SR)&FLASH_SR_BSY);
  FLASH->CR |= FLASH_CR_PG; // FLASH programming enable
  FLASH->CR &= ~FLASH_CR_PSIZE_0; // 32-bit parallelism
  FLASH->CR |= FLASH_CR_PSIZE_1; // 32-bit parallelism
  while((FLASH->SR)&FLASH_SR_BSY);
  HWREG(address) = data; // Write data
  while((FLASH->SR)&FLASH_SR_BSY);
  FLASH->CR &= ~FLASH_CR_PG; // FLASH programming enable
}