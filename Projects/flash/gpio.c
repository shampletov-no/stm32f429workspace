// GPIO init functions
#include "stm32f429xx.h"

// Definitions
void InitGPIO(){
  // PG 14 mode output (red LED)
  GPIOG->MODER |= GPIO_MODER_MODER14_0;
  GPIOG->MODER &= ~GPIO_MODER_MODER14_1;
  // PA 0 mode input (Button)
  GPIOA->MODER &= ~GPIO_MODER_MODER0;
}